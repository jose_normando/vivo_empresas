export const state = () => ({
  status: null
})

export const mutations = {
  setStatus (state, boolean) {
    state.status = boolean
  }
}