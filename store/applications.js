export const state = () => ({
  application: null
})

export const mutations = {
  setApplication(state, application) {
    state.application = application
  }
}