export const state = () => ({
    displayCoverageModal: false,
    displayConfirmationModal: false,
    displaySuccessCoverageModal: false,
    displayApplicationModal: false,
    displayChannelsModal: false,
    displayInfoCardModal: false,
    onlyNumber: false,
    displaySearchByAddressModal: false,
  })
  
  export const mutations = {
    openCoverageModal(state) {
      state.displayCoverageModal = true
    },
    closeCoverageModal(state) {
      state.displayCoverageModal = false
    },

    openConfirmationModal(state) {
      state.displayConfirmationModal = true
    },
    closeConfirmationModal(state) {
      state.displayConfirmationModal = false
    },

    openSuccessCoverageModal(state) {
      state.displaySuccessCoverageModal = true
    },
    closeSuccessCoverageModal(state) {
      state.displaySuccessCoverageModal = false
    },

    openApplicationModal(state) {
      state.displayApplicationModal = true
    },
    closeApplicationModal(state) {
      state.displayApplicationModal = false
    },

    openChannelsModal(state) {
      state.displayChannelsModal = true
    },
    closeChannelsModal(state) {
      state.displayChannelsModal = false
    },

    openInfoCardModal(state) {
      console.log(state)
      state.displayInfoCardModal = true
    },
    closeInfoCardModal(state) {
      state.displayInfoCardModal = false
    },
    
    setOnlyNumber(state) {
      state.onlyNumber = true
    },
    unsetOnlyNumber(state) {
      state.onlyNumber = false
    },

    setSearchByAddressModal(state, boolean) {
      state.displaySearchByAddressModal = boolean
    },
  }
  