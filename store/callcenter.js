const mountQuery = (coverage, route) => {
    let query = ''
    if (coverage.utm_campaign) {
      query += `/${coverage.utm_campaign}`
    }
    return query
  }
  
  export const state = () => ({
    isCallCenterDisabled: null,
    phone_number: null,
    status: null,
  })
  
  export const mutations = {
    setIsCallCenterDisabled (state, status) {
      state.isCallCenterDisabled = status
    },
  
    setPhone (state, phone_number) {
      state.phone_number = phone_number
    },
  
    setIsPhoneActive (state, isPhoneActive) {
      state.status = isPhoneActive
    }
  }
  
  export const actions = {
    async getCallCenterStatus () {
      let response = await this.$axios('/click-to-call')
      this.commit('callcenter/setIsCallCenterDisabled', response.data.offline)
    },
  
    async getCallCenterContactInfo () {
      let response = await this.$axios(`/call-center${mountQuery(this.state.plans.coverage, this.$router.app._route)}`)
      this.commit('callcenter/setIsPhoneActive', response.data.status)
      this.commit('callcenter/setPhone', response.data.phone_number)
    }
  }