export const state = () => ({
    address: {}
  })
  
  const setUtms = coverage => {
    let query = `&utm_campaign=${coverage.utm_campaign}`
    query += `&utm_medium=${coverage.utm_medium}`
    query += `&utm_source=${coverage.utm_source}`
    query += `&group=${coverage.utm_content}`
    query += `&gclid=${coverage.gclid}`
    return query
  }
  
  export const mutations = {
    setForms(state, forms) {
        this.state.forms = {...forms}
    },
  
    setAddress(state, address) {
        state.address = address
        sessionStorage.setItem('address', JSON.stringify(address))
    }
  }
  
  export const actions = {
    async fetchForms() {
      let response = await this.$axios(
        `/form/${this.state.plans.coverage.utm_source || ''}`
      )
      this.commit('forms/setForms', response.data)
    }
  }
  
  
  export const getters = {
    getForms (state) {
      return state
    },
  }