export const state = () => ({
  lead_id: null,
  postal_code: null,
  number: null,
  access_leads_id: null
})

export const mutations = {
  setLead(state, lead) {
    state.lead_id = lead.lead_id
    state.postal_code = lead.postal_code
    state.number = lead.number
    state.access_leads_id = lead.access_leads_id
    sessionStorage.setItem('leads', JSON.stringify(lead))
  },
  setAccessLeadId(state, id){
    state.access_leads_id = id
    sessionStorage.setItem('leads', JSON.stringify(state))
  }
}