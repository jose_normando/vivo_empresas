export const actions = {
  async fetchPageContent({},params) {
    const { category, content } = params
    const { data } =  await this.$axios(`pages/produto/${category}/${content}`)
    return data
  },
}

