 const setUtms = coverage => {
   let query = `&utm_campaign=${coverage.utm_campaign}`
   query += `&utm_medium=${coverage.utm_medium}`
   query += `&utm_source=${coverage.utm_source}`
   query += `&group=${coverage.utm_content}`
   query += `&gclid=${coverage.gclid}`
   return query
 }
 
 const mountQuery = (coverage, route) => {
   let query = ''
   const { state, slug } = route.params
 
    if (Object.keys(route.query).length > 0 &&
       !coverage.postal_code &&
       !coverage.number
    ) {
        query = '?uf=SP'
        for (let property in route.query) {
            if(property == 'utm_campaign'){
                if (route.query[property] == 'ftthmarca_marte_1' || 
                    route.query[property] == 'ftthmarca_marte_2' || 
                    route.query[property] == 'ftthmarca_marte_3') 
                {
                    query += `&region=${route.query[property].replaceAll('_', '-')}`

                } 
                if (route.query[property] == 'ftthmarca' || 
                    route.query[property] == 'ftthmarca_seg' || 
                    route.query[property] == 'ftthfibra_gen'|| 
                    route.query[property] == 'spmetalico_marca') 
                {
                    query += `&region=200mb`

                } else {
                    query += `&${property}=${route.query[property]}`
                }

            } else {
                query += `&${property}=${route.query[property]}`
            }
        }
    
    } else if (coverage.postal_code && coverage.number) {
        query = `?postal_code=${coverage.postal_code}`
        query += `&number=${coverage.number}`

        for (let property in route.query) {
            query += `&${property}=${route.query[property]}`
        }
 
    } else if (state && slug) {
        query = `?uf=${state.toUpperCase()}`
    
    } else {
        query += `?uf=SP`
    }
 
    let fbc = document.cookie
        .split(';')
        .map(x => x.split('='))
        .find(x => x[0] === ' _fbc')

    if (fbc) { fbc.length > 0 ? (query += `&fbc=${fbc[1]}`) : "" }
 
    return query
 }
 
 export const state = () => ({
   isFetching: false,
   hasCoverage: false,
   coverage: {
     phone_number: null,
     postal_code: null,
     number: null,
     uf: null,
   },
   types: null,
   products: {},
   timeout: false,
 })
 
 export const actions = {
   async getPlans ({ state, dispatch }) {
     this.commit('plans/toggleIsFetching')
 
     try {
       let plansNew, leadNew, timeoutNew
   
       let { plans, lead, timeout } = await this.$axios.$get(
         `/plans${mountQuery(state.coverage, this.$router.app._route)}`, 
         {
           headers: {
            'x-origin': 'https://planosvivointernet.com.br',
            Accept: 'application/json'
           }
         }
       )
       plansNew = plans
       leadNew = lead
       timeoutNew = timeout
       
       this.commit('plans/setProducts', plansNew)
       this.commit('plans/setCoverageUf', leadNew.state)
       this.commit('plans/setTimeout', timeoutNew)
     
       
       if (plansNew.length === 0) {
         this.commit('plans/setHasCoverage', false)
         let { plans, lead, timeout } = await this.$axios.$get(`/plans?uf=SP`,
         {
          headers: {
           'x-origin': 'https://planosvivointernet.com.br',
           Accept: 'application/json'
          }
        })
         this.commit('plans/setProducts', plans)
         this.commit('plans/setTimeout', timeout)
       } else {
         if (state.coverage.number && state.coverage.postal_code) {
           this.commit('plans/setHasCoverage', true)
         }

       }
     } catch (error) {
       this.commit('plans/setHasCoverage', false)
 
       let { plans, lead, timeout } = await this.$axios.$get(`/plans?uf=SP`,
       {
        headers: {
         'x-origin': 'https://planosvivointernet.com.br',
         Accept: 'application/json'
        }
      })
       this.commit('plans/setProducts', plans)
       this.commit('plans/setTimeout', timeout)
     }
     this.commit('plans/toggleIsFetching')
   },
 
   async getEcommerce ({ state }) {
     try {
       let data = await this.$axios.$get(
         `/ecommerce/${state.coverage.utm_source || 'org'}`
       )
 
       this.commit(
         'ecommerce/setStatus',
         data['25239f5d-9d48-41ce-8a6d-fad2c07cb0e3'].active
       )
       // this.commit('plans/setProducts', reducePlans(plans))
     } catch (error) {
       // this.commit('plans/setProducts', {})
     }
   },
 
   returnAlacarteId ({ state }, alacarteShortDescription) {
     const plans = state.products
     if (plans['a-la-carte'] && plans['a-la-carte'].length > 0) {
       const alacarte = plans['a-la-carte'].filter(plan => {
         return plan.short_description === alacarteShortDescription
       })
       if(alacarte.length > 0){
         return alacarte[0].id
       }else{
         return null
       }
     }
   }
 }
 
export const mutations = {
    setProducts (state, obj) {
        state.products = obj
        sessionStorage.setItem('products', JSON.stringify(obj))
    },
    
    setCoverage (state, obj) {
        let coverage = {
        ...state.coverage,
        ...obj
        }
    
        sessionStorage.setItem('coverage', JSON.stringify(coverage))
        state.coverage = coverage
    },
    
    setCoverageUf (state, uf) {
        let obj = {
        ...state.coverage,
        uf
        }
    
        sessionStorage.setItem('coverage', JSON.stringify(obj))
        state.coverage = { ...obj }
    },
    
    toggleIsFetching (state) {
        state.isFetching = !state.isFetching
    },
    
    setTimeout (state, timeout) {
        if (typeof timeout !== 'undefined') {
        state.timeout = timeout
        }
    },
    
    setHasCoverage (state, boolean) {
        state.hasCoverage = boolean
        sessionStorage.setItem('hasCoverage', boolean)
    },
 
}
 export const getters  = {
   getPlans : state => state.products,
 }