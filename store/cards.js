export const state = () => ({
  displayInfo: false,
  idToCheckCoverage: null
})

export const mutations = {
  setDisplayInfo(state) {
    state.displayInfo = !state.displayInfo
  },

  setIdToCheckCoverage(state, planId) {
    state.idToCheckCoverage = planId
  }
}
