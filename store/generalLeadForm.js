export const state = () => ({
    displayFormModal: false,
    hasSendGeneralLead: false
  })
  
  export const mutations = {
    openFormModal (state) {
      state.displayFormModal = true
    },
  
    closeFormModal (state) {
      state.displayFormModal = false
    },
  
    setHasSendGeneralLead (state, hasSend) {
      sessionStorage.setItem('hasSendGeneralLead', hasSend)
      state.hasSendGeneralLead = hasSend
    }
  }