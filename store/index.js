const setUtms = coverage => {
  let query = `&utm_campaign=${coverage.utm_campaign}`
  query += `&utm_medium=${coverage.utm_medium}`
  query += `&utm_source=${coverage.utm_source}`
  query += `&group=${coverage.utm_content}`
  query += `&gclid=${coverage.gclid}`
  return query
}

const mountQuery = (coverage, route) => {
  let query = ''
  const { state } = route.params

  if (
    Object.keys(route.query).length > 0 &&
    !coverage.postal_code &&
    !coverage.number
  ) {
    query = '?uf=SP'
    for (let property in route.query) {
      query += `&${property}=${route.query[property]}`
    }

    if (!query.includes('utm_source')) {
      query += setUtms(coverage)
    }
  } else if (coverage.postal_code && coverage.number) {
    query = `?postal_code=${coverage.postal_code}`
    query += `&number=${coverage.number}`
    query += setUtms(coverage)
  } else if (state) {
    query = `?uf=${state.toUpperCase()}`
  } else {
    query += `?uf=SP`
  }

  return query
}

export const state = () => ({
  banners: {
    data: null
  },

  categories: null,
  error: null,
  messageError:{ message:'Page not found', statusCode:404 }
})
export const mutations = {
  setBanner (state, banner) {
    state.banners.data = banner
  },

  setCategories (state, categories) {
    state.categories = categories
  },

  setCoverageUf (state, uf) {
    let obj = {
      ...state.plans.coverage,
      uf
    }

    state.plans.coverage = { ...obj }

    if (process.browser) {
      sessionStorage.setItem('plans', JSON.stringify(state.plans))
    }
  },

  setForms (state, forms) {
    state.forms = forms 
  },

  setPhone (state, phone_number) {
    if(process.browser){
      if(!sessionStorage.getItem('phone')){
      state.callcenter.phone_number = phone_number
        sessionStorage.setItem('phone', phone_number)
      } else{
        state.callcenter.phone_number =  sessionStorage.getItem('phone')
      }
    }
  },

  setIsPhoneActive (state, isPhoneActive) {
    state.callcenter.status = isPhoneActive
  },

  setIsCallCenterDisabled (state, status) {
    state.callcenter.isCallCenterDisabled = status
  },
  
  setStatus (state, boolean) {
    state.ecommerce.status = boolean
  },

  setError (state, payload) {
    state.error = payload
  }
}


export const actions = { 
  async nuxtServerInit (vuexContext, context) {
    const { utm_source } = context.route.query
    let formQuery = `/form`
    utm_source ? formQuery += `?utm_source=${utm_source}` : null

    const forms = await context.app.$axios.$get(formQuery)
    vuexContext.commit('forms/setForms', forms)

    const clickToCall = await context.app.$axios.$get('/click-to-call')
    vuexContext.commit('setIsCallCenterDisabled', clickToCall.offline)
  },

  async getCallCenter({ commit }){
    const { utm_medium } = this.$router.app.context.query
    const callCenter = await this.$axios.$get(`/call-center${utm_medium ? '/'+ utm_medium : ''}`)
    commit('setIsPhoneActive', callCenter.status)
    commit('setPhone', callCenter.phone_number)
  }
}
export const getters = {
  getBanner (state) {
    return state.banners.data
  },

  getIsPhoneActive (state) {
    return state.callcenter.status
  },

  getPhone (state) {
    return state.callcenter.phone_number
  },

  getCallCenterStatus (state) {
    return state.callcenter.isCallCenterDisabled
  },

  getCategories (state) {
    return state.categories
  },
  }
