export const state = () => ({
  data: [
  
  ],
  planName: ''
})

export const mutations = {
  setChannels(state, channels) {
    state.data = channels
  },

  setPlanName(state, planName) {
    state.planName = planName
  }
}
