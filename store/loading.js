export const state = () => ({
  isLoading: false
})

export const mutations = {
  toggleLoading(state) {
    state.isLoading = !state.isLoading
  }
}