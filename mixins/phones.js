export default {
    methods: {
      async isPhoneValid (phone) {
        const { data } = await this.$axios(`/phone_number/validate?phone_number=${phone}`)
        return data.valid
      },
  
      hasInvalidDigits (phone) {
        const phoneArray = phone.split('')
        const noAreaCodeArray = phoneArray.splice(2)
        const stringPhone = noAreaCodeArray.join('')
        const firstDigit = stringPhone.charAt(0)
  
        if (firstDigit >= 2 && firstDigit <= 5) {
          if (stringPhone.length !== 8) {
            return true
          }
        }
        else if (firstDigit >= 6 && firstDigit <= 9) {
          if (stringPhone.length !== 9) {
            return true
          }
        }
  
        return stringPhone.match(/^(\d)\1{8}/g)
      }
    }
  }