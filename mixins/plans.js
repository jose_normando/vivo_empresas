export default {
    computed: {
      isFetchingPlans() {
        return this.$store.state.plans.isFetching
      },
  
      internet() {
        let internet = []
        let products = this.$store.state.plans.products
  
        if (products && products.internet) {
          internet = products.internet.filter(plan => {
            return plan.download_speed_in_mb < 50
          })
        }
  
        return internet
      },
  
      fibra() {
        let fibra = []
        let products = this.$store.state.plans.products
  
        if (products && products.internet) {
          fibra = products.internet.filter(plan => {
            return plan.download_speed_in_mb >= 50
          })
        }
  
        return fibra
      },
      combos() {
        let combos = []
  
        if (this.$store.state.plans.products && this.$store.state.plans.products.combo) {
          combos = this.$store.state.plans.products.combo
        }
  
        return combos
      }
    }
  }
  