export default {
  computed: {
    combos() {
      let combos = {}
      let netFixoTv = []
      let netTv = []
      let netFixo = []
      let internet = []

      const products = this.$store.state.plans.products

      if (products && products.combo) {
        combos = [...products.combo]
        if ('internet' in products) {
          internet = [...products.internet]
        }

        combos.forEach(combo => {
          switch (combo.combo_types) {
            case 'Internet + Fixo':
              netFixo.push(combo)
              break

            case 'Internet + TV':
              netTv.push(combo)
              break

            case 'Internet + Fixo + TV':
              netFixoTv.push(combo)
              break

            default:
              break
          }

        })

        if (netFixoTv.length > 0) {
          netFixoTv.sort(function(a, b) {
            return 'weight' in a && 'weight' in b ? b.weight - a.weight : ''
          })
        }

        if (netTv.length > 0) {
          netTv.sort(function(a, b) {
            return 'weight' in a && 'weight' in b ? b.weight - a.weight : ''
          })
        }

        if (netFixo.length > 0) {
          netFixo.sort(function(a, b) {
            return 'weight' in a && 'weight' in b ? b.weight - a.weight : ''
          })
        }

        if (internet.length > 0) {
          internet.sort(function(a, b) {
            return 'weight' in a && 'weight' in b ? b.weight - a.weight : ''
          })
        }

        const { utm_campaign } = this.$route.query


        combos = {
          netFixoTv,
          netTv,
          netFixo,
          internet,
        }
      }

      return combos
    }
  },

  methods: {
    isUltimateHD(product) {
      if (product.plans && product.plans.length > 0) {
        const ultimate = product.plans.filter(plan => {
          return plan.short_description === 'Ultimate HD'
        })

        return ultimate.length > 0
      }
    }
  }
}
