import getCookies from '@/mixins/cookies'

export default {
   mixins: [getCookies],
  methods: {
    /**
     * Generates new lead if has none on store or 
     * if postal code and number is different from stored on vuex store
     */
    async generateLead (leadType) {
      if(this.leads){
        if (this.leads.lead_id) {
        if (
          this.leads.postal_code != this.coverage.postal_code ||
          this.leads.number != this.coverage.number
        ) {
          await this.setLead(leadType)
        }
      } else {
        await this.setLead(leadType)
      }
    }else{
      await this.setLead(leadType)
    }
    },
 
    /**
     * Set lead_id on store and sessionStorage
     */
    async setLead (leadType) {
      let url = window.location.href
      url = url.substring(0 , url.indexOf('?'))
      
      const { lead_id } = await this.$axios.$post('/lead/basic', {
        url: url,
        type: leadType,
        postal_code: this.coverageInfo.postal_code,
        addr_number: this.coverageInfo.number,
        phone_number: this.coverageInfo.phone_number,
        ...this.$router.app._route.query
      })
      
      await this.$store.commit('leads/setLead', {
          lead_id,
          type: leadType,
          postal_code: this.coverageInfo.postal_code,
          number: this.coverageInfo.number,
          phone_number: this.coverageInfo.phone_number
        })
        
      this.getCookies();
    }
  }
}