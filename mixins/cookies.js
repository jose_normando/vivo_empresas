export default {
    methods: {
      async getCookies() {
  
        let cook = document.cookie.replaceAll(/\s/g,'')
        let FirstSession;
        let ReturningSession;
  
        FirstSession = cook.split(';')
        .map(x => x.split('='))
        .find(x => x[0] === 'FirstSession')
    
        ReturningSession = cook.split(';')
        .map(x => x.split('='))
        .find(x => x[0] === 'ReturningSession')
  
        let itemFirst = [];
        let itemReturn = []
  
        if(FirstSession){
          FirstSession = decodeURIComponent(unescape(FirstSession[1])).split("&");
      
          console.log("Primeira seção",FirstSession)
  
          for (let property in FirstSession) {
            itemFirst[FirstSession[property].split("=")[0]] = FirstSession[property].split("=")[1]
          }
        }
  
        if(ReturningSession){
         ReturningSession = decodeURIComponent(unescape(ReturningSession[1])).split("&");
          console.log("Retorno seção",ReturningSession)
  
          for (let property in ReturningSession) {
            itemReturn[ReturningSession[property].split("=")[0]] = ReturningSession[property].split("=")[1]
          }
        }
      
  
        if(Object.keys(itemFirst).length > 0){
          let obj = itemFirst;
  
          if(Object.keys(itemReturn).length > 0 &&  this.$store.state.leads.access_leads_id){
            obj = itemReturn;
          }
          
          if(this.$store.state.leads.lead_id){
            console.log(this.$store.state)
            let parent =  this.$store.state.leads.lead_id
            let type = 'first'
            let parent_type = 'lead'
            if(this.$store.state.leads.access_leads_id){
              parent = this.$store.state.leads.access_leads_id
              type = 'navigation'
              parent_type = 'navigation'
            } 
            let item = {
              type:type,
              ...obj,
              parent_type: parent_type,
              parent_id: parent,
             
            }
            
            let data = await this.$axios.$post('/lead/access', item)
            if(data.access){
              this.$store.commit('leads/setAccessLeadId', data.access)
            }
          }
        }
      }
    }
  }
  