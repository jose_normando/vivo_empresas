export default {
  methods: {
    /**
     * fetch banner according to route path
     */
    async fetchBanners() {
      try {
        let url = `/banners${
          this.$route.path == '/' ? '/home' : this.$route.path
        }`

        let state = this.$store.state.plans.coverage.uf

        if (state !== 'SP' && state != undefined) {
          url += `?state=${state}`
        }

        let { data } = await this.$axios(url)
        this.$store.commit('setBanner', data)
      } catch (error) {
        console.log(error)
      }
    }
  }
}
