export default {
    methods: {
        async isPostalCodeValid(postal_code) {
        const { data } = await this.$axios(`/location/valid-postal-code?postal_code=${postal_code}`)
        return data.valid
        }
    }
}