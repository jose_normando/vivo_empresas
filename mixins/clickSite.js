export default {
    computed: {
      isCallCenterDisabled() {
        return this.$store.state.callcenter.isCallCenterDisabled
      },
      leads() {
        return this.$store.state.leads
      },
    },
    methods: {
      async callToCustomer(val) {
        let click = 'clicksite'
        if (!this.isCallCenterDisabled) {
          if (val == 'now') {
            click = 'clicksitenow'
          } else if (val == 'vivopfsemcobsite') {
            click = 'vivopfsemcobsite'
          }
  
          let lead = {
            type: click
          }
          console.log("Chegou do modal Success", val, this.lead)
          if (this.leads.lead_id) {
            lead.lead_id = this.leads.lead_id
          }
  
          for (let property in this.coverageInfo) {
            if (this.coverageInfo[property]) {
              lead[property] = this.coverageInfo[property]
            }
          }
  
          const { lead_id } = await this.$axios.$post('/click-site', lead)
  
          if (lead_id !== this.leads.lead_id) {
            this.$store.commit('leads/setLead', lead)
          }
  
          
        }
      }
    }
  }
  