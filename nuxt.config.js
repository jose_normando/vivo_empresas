export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'vivo-pj',
    htmlAttrs: {
      lang: 'pt-br'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Descrição do Vivo Empresas' }
    ],
    link: [
     
    ]
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/scss/app.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '@/plugins/VueSwiperAwesome', mode: 'client' },
    { src: '~/plugins/VueMask' },
    { src: '~/plugins/gtm' }

  ],
  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/sitemap',
    'nuxt-webfontloader',
  ],

  webfontloader: {
    google: {
      families: ['Roboto:300,400,500,700&display=swap'] 
    }
  }, 
  
  axios: {
    baseURL: process.env.API_URL,
    headers: {
      common: {
        'x-origin': 'https://portalvivoempresas.com.br',
        Accept: 'application/json'
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
}
