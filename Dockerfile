FROM node:14.15.0

ENV APP_DIR /app/

WORKDIR ${APP_DIR}

COPY . ${APP_DIR}

# RUN cp ${APP_DIR}.env.example .env

RUN yarn install --verbose

RUN yarn build --modern

ENV HOST 0.0.0.0   # Insensitive environment variable

EXPOSE 80

CMD ["yarn", "start","--port=80","--modern"]
