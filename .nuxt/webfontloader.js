
import WebFont from 'webfontloader'

const options = {"google":{"families":["Roboto:300,400,500,700&display=swap"]}}

WebFont.load(options)
