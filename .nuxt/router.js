import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _3e1843e5 = () => interopDefault(import('../pages/blog/index.vue' /* webpackChunkName: "pages/blog/index" */))
const _1a237ba8 = () => interopDefault(import('../pages/produto/index.vue' /* webpackChunkName: "pages/produto/index" */))
const _c94654a0 = () => interopDefault(import('../pages/vivo-combo/index.vue' /* webpackChunkName: "pages/vivo-combo/index" */))
const _3d5eaba4 = () => interopDefault(import('../pages/vivo-fixo/index.vue' /* webpackChunkName: "pages/vivo-fixo/index" */))
const _a68fcd96 = () => interopDefault(import('../pages/vivo-internet/index.vue' /* webpackChunkName: "pages/vivo-internet/index" */))
const _33c20140 = () => interopDefault(import('../pages/blog/_slug/index.vue' /* webpackChunkName: "pages/blog/_slug/index" */))
const _470a1a7a = () => interopDefault(import('../pages/produto/_category/index.vue' /* webpackChunkName: "pages/produto/_category/index" */))
const _7388c876 = () => interopDefault(import('../pages/produto/_category/_content/index.vue' /* webpackChunkName: "pages/produto/_category/_content/index" */))
const _7165ac7c = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/blog",
    component: _3e1843e5,
    name: "blog"
  }, {
    path: "/produto",
    component: _1a237ba8,
    name: "produto"
  }, {
    path: "/vivo-combo",
    component: _c94654a0,
    name: "vivo-combo"
  }, {
    path: "/vivo-fixo",
    component: _3d5eaba4,
    name: "vivo-fixo"
  }, {
    path: "/vivo-internet",
    component: _a68fcd96,
    name: "vivo-internet"
  }, {
    path: "/blog/:slug",
    component: _33c20140,
    name: "blog-slug"
  }, {
    path: "/produto/:category",
    component: _470a1a7a,
    name: "produto-category"
  }, {
    path: "/produto/:category/:content",
    component: _7388c876,
    name: "produto-category-content"
  }, {
    path: "/",
    component: _7165ac7c,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
