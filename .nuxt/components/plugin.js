import Vue from 'vue'
import { wrapFunctional } from './utils'

const components = {
  AccordionCategoryItems: () => import('../../components/AccordionCategoryItems.vue' /* webpackChunkName: "components/accordion-category-items" */).then(c => wrapFunctional(c.default || c)),
  AccordionProducts: () => import('../../components/AccordionProducts.vue' /* webpackChunkName: "components/accordion-products" */).then(c => wrapFunctional(c.default || c)),
  ApplicationModal: () => import('../../components/ApplicationModal.vue' /* webpackChunkName: "components/application-modal" */).then(c => wrapFunctional(c.default || c)),
  BenefitsVivo: () => import('../../components/BenefitsVivo.vue' /* webpackChunkName: "components/benefits-vivo" */).then(c => wrapFunctional(c.default || c)),
  BlogPosts: () => import('../../components/BlogPosts.vue' /* webpackChunkName: "components/blog-posts" */).then(c => wrapFunctional(c.default || c)),
  BreadCrumb: () => import('../../components/BreadCrumb.vue' /* webpackChunkName: "components/bread-crumb" */).then(c => wrapFunctional(c.default || c)),
  Footer: () => import('../../components/Footer.vue' /* webpackChunkName: "components/footer" */).then(c => wrapFunctional(c.default || c)),
  FormCobertura: () => import('../../components/FormCobertura.vue' /* webpackChunkName: "components/form-cobertura" */).then(c => wrapFunctional(c.default || c)),
  FormModal: () => import('../../components/FormModal.vue' /* webpackChunkName: "components/form-modal" */).then(c => wrapFunctional(c.default || c)),
  Header: () => import('../../components/Header.vue' /* webpackChunkName: "components/header" */).then(c => wrapFunctional(c.default || c)),
  InfoCardModal: () => import('../../components/InfoCardModal.vue' /* webpackChunkName: "components/info-card-modal" */).then(c => wrapFunctional(c.default || c)),
  KnowPlans: () => import('../../components/KnowPlans.vue' /* webpackChunkName: "components/know-plans" */).then(c => wrapFunctional(c.default || c)),
  MainBanner: () => import('../../components/MainBanner.vue' /* webpackChunkName: "components/main-banner" */).then(c => wrapFunctional(c.default || c)),
  MeetBlog: () => import('../../components/MeetBlog.vue' /* webpackChunkName: "components/meet-blog" */).then(c => wrapFunctional(c.default || c)),
  Modal: () => import('../../components/Modal.vue' /* webpackChunkName: "components/modal" */).then(c => wrapFunctional(c.default || c)),
  Product: () => import('../../components/Product.vue' /* webpackChunkName: "components/product" */).then(c => wrapFunctional(c.default || c)),
  ProductCoverage: () => import('../../components/ProductCoverage.vue' /* webpackChunkName: "components/product-coverage" */).then(c => wrapFunctional(c.default || c)),
  Questions: () => import('../../components/Questions.vue' /* webpackChunkName: "components/questions" */).then(c => wrapFunctional(c.default || c)),
  SearchByAddressModal: () => import('../../components/SearchByAddressModal.vue' /* webpackChunkName: "components/search-by-address-modal" */).then(c => wrapFunctional(c.default || c)),
  SpeakNowBanner: () => import('../../components/SpeakNowBanner.vue' /* webpackChunkName: "components/speak-now-banner" */).then(c => wrapFunctional(c.default || c)),
  SuccessCoverageModal: () => import('../../components/SuccessCoverageModal.vue' /* webpackChunkName: "components/success-coverage-modal" */).then(c => wrapFunctional(c.default || c)),
  TabProducts: () => import('../../components/TabProducts.vue' /* webpackChunkName: "components/tab-products" */).then(c => wrapFunctional(c.default || c)),
  TabProductsCoverage: () => import('../../components/TabProductsCoverage.vue' /* webpackChunkName: "components/tab-products-coverage" */).then(c => wrapFunctional(c.default || c))
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}
