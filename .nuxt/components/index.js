import { wrapFunctional } from './utils'

export { default as AccordionCategoryItems } from '../../components/AccordionCategoryItems.vue'
export { default as AccordionProducts } from '../../components/AccordionProducts.vue'
export { default as ApplicationModal } from '../../components/ApplicationModal.vue'
export { default as BenefitsVivo } from '../../components/BenefitsVivo.vue'
export { default as BlogPosts } from '../../components/BlogPosts.vue'
export { default as BreadCrumb } from '../../components/BreadCrumb.vue'
export { default as Footer } from '../../components/Footer.vue'
export { default as FormCobertura } from '../../components/FormCobertura.vue'
export { default as FormModal } from '../../components/FormModal.vue'
export { default as Header } from '../../components/Header.vue'
export { default as InfoCardModal } from '../../components/InfoCardModal.vue'
export { default as KnowPlans } from '../../components/KnowPlans.vue'
export { default as MainBanner } from '../../components/MainBanner.vue'
export { default as MeetBlog } from '../../components/MeetBlog.vue'
export { default as Modal } from '../../components/Modal.vue'
export { default as Product } from '../../components/Product.vue'
export { default as ProductCoverage } from '../../components/ProductCoverage.vue'
export { default as Questions } from '../../components/Questions.vue'
export { default as SearchByAddressModal } from '../../components/SearchByAddressModal.vue'
export { default as SpeakNowBanner } from '../../components/SpeakNowBanner.vue'
export { default as SuccessCoverageModal } from '../../components/SuccessCoverageModal.vue'
export { default as TabProducts } from '../../components/TabProducts.vue'
export { default as TabProductsCoverage } from '../../components/TabProductsCoverage.vue'

export const LazyAccordionCategoryItems = import('../../components/AccordionCategoryItems.vue' /* webpackChunkName: "components/accordion-category-items" */).then(c => wrapFunctional(c.default || c))
export const LazyAccordionProducts = import('../../components/AccordionProducts.vue' /* webpackChunkName: "components/accordion-products" */).then(c => wrapFunctional(c.default || c))
export const LazyApplicationModal = import('../../components/ApplicationModal.vue' /* webpackChunkName: "components/application-modal" */).then(c => wrapFunctional(c.default || c))
export const LazyBenefitsVivo = import('../../components/BenefitsVivo.vue' /* webpackChunkName: "components/benefits-vivo" */).then(c => wrapFunctional(c.default || c))
export const LazyBlogPosts = import('../../components/BlogPosts.vue' /* webpackChunkName: "components/blog-posts" */).then(c => wrapFunctional(c.default || c))
export const LazyBreadCrumb = import('../../components/BreadCrumb.vue' /* webpackChunkName: "components/bread-crumb" */).then(c => wrapFunctional(c.default || c))
export const LazyFooter = import('../../components/Footer.vue' /* webpackChunkName: "components/footer" */).then(c => wrapFunctional(c.default || c))
export const LazyFormCobertura = import('../../components/FormCobertura.vue' /* webpackChunkName: "components/form-cobertura" */).then(c => wrapFunctional(c.default || c))
export const LazyFormModal = import('../../components/FormModal.vue' /* webpackChunkName: "components/form-modal" */).then(c => wrapFunctional(c.default || c))
export const LazyHeader = import('../../components/Header.vue' /* webpackChunkName: "components/header" */).then(c => wrapFunctional(c.default || c))
export const LazyInfoCardModal = import('../../components/InfoCardModal.vue' /* webpackChunkName: "components/info-card-modal" */).then(c => wrapFunctional(c.default || c))
export const LazyKnowPlans = import('../../components/KnowPlans.vue' /* webpackChunkName: "components/know-plans" */).then(c => wrapFunctional(c.default || c))
export const LazyMainBanner = import('../../components/MainBanner.vue' /* webpackChunkName: "components/main-banner" */).then(c => wrapFunctional(c.default || c))
export const LazyMeetBlog = import('../../components/MeetBlog.vue' /* webpackChunkName: "components/meet-blog" */).then(c => wrapFunctional(c.default || c))
export const LazyModal = import('../../components/Modal.vue' /* webpackChunkName: "components/modal" */).then(c => wrapFunctional(c.default || c))
export const LazyProduct = import('../../components/Product.vue' /* webpackChunkName: "components/product" */).then(c => wrapFunctional(c.default || c))
export const LazyProductCoverage = import('../../components/ProductCoverage.vue' /* webpackChunkName: "components/product-coverage" */).then(c => wrapFunctional(c.default || c))
export const LazyQuestions = import('../../components/Questions.vue' /* webpackChunkName: "components/questions" */).then(c => wrapFunctional(c.default || c))
export const LazySearchByAddressModal = import('../../components/SearchByAddressModal.vue' /* webpackChunkName: "components/search-by-address-modal" */).then(c => wrapFunctional(c.default || c))
export const LazySpeakNowBanner = import('../../components/SpeakNowBanner.vue' /* webpackChunkName: "components/speak-now-banner" */).then(c => wrapFunctional(c.default || c))
export const LazySuccessCoverageModal = import('../../components/SuccessCoverageModal.vue' /* webpackChunkName: "components/success-coverage-modal" */).then(c => wrapFunctional(c.default || c))
export const LazyTabProducts = import('../../components/TabProducts.vue' /* webpackChunkName: "components/tab-products" */).then(c => wrapFunctional(c.default || c))
export const LazyTabProductsCoverage = import('../../components/TabProductsCoverage.vue' /* webpackChunkName: "components/tab-products-coverage" */).then(c => wrapFunctional(c.default || c))
