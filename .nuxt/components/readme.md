# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<AccordionCategoryItems>` | `<accordion-category-items>` (components/AccordionCategoryItems.vue)
- `<AccordionProducts>` | `<accordion-products>` (components/AccordionProducts.vue)
- `<ApplicationModal>` | `<application-modal>` (components/ApplicationModal.vue)
- `<BenefitsVivo>` | `<benefits-vivo>` (components/BenefitsVivo.vue)
- `<BlogPosts>` | `<blog-posts>` (components/BlogPosts.vue)
- `<BreadCrumb>` | `<bread-crumb>` (components/BreadCrumb.vue)
- `<Footer>` | `<footer>` (components/Footer.vue)
- `<FormCobertura>` | `<form-cobertura>` (components/FormCobertura.vue)
- `<FormModal>` | `<form-modal>` (components/FormModal.vue)
- `<Header>` | `<header>` (components/Header.vue)
- `<InfoCardModal>` | `<info-card-modal>` (components/InfoCardModal.vue)
- `<KnowPlans>` | `<know-plans>` (components/KnowPlans.vue)
- `<MainBanner>` | `<main-banner>` (components/MainBanner.vue)
- `<MeetBlog>` | `<meet-blog>` (components/MeetBlog.vue)
- `<Modal>` | `<modal>` (components/Modal.vue)
- `<Product>` | `<product>` (components/Product.vue)
- `<ProductCoverage>` | `<product-coverage>` (components/ProductCoverage.vue)
- `<Questions>` | `<questions>` (components/Questions.vue)
- `<SearchByAddressModal>` | `<search-by-address-modal>` (components/SearchByAddressModal.vue)
- `<SpeakNowBanner>` | `<speak-now-banner>` (components/SpeakNowBanner.vue)
- `<SuccessCoverageModal>` | `<success-coverage-modal>` (components/SuccessCoverageModal.vue)
- `<TabProducts>` | `<tab-products>` (components/TabProducts.vue)
- `<TabProductsCoverage>` | `<tab-products-coverage>` (components/TabProductsCoverage.vue)
